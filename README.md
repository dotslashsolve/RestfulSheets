# RestfulSheets

## Install

*NOTE* this project only supports AWS because of the `serverless-domain-manager`
plugin. If you're using a different service, you'll need to remove the plugin
and the domain related custom variables at the bottom of `serverless.yml`.

#### Serverless

You'll need to have `serverless` installed and configured globally in order to run
this project. For details see
[getting-started](https://serverless.com/framework/docs/getting-started/).

#### Google Cloud API

This project requires access to google sheets and as such needs an api key.
Assuming you already have an account, go to 'APIs & Services' > 'Enable APIs' and
search for sheets. Enable this API and then go to 'credentials' >
'create credentials' > 'API Key'. Name the key whatever you want, but you should
make sure to set the restrictions to Google Sheets Read and Write only. Copy the
key and save it to your `.env` file.

#### Google Sheets

You'll also need to create a goole sheet for usage with this project. Simply
create a new sheet and then go to share and set it so that 'anyone with a link'
can edit.

#### Environment

Clone or copy this repo the usual way. You'll need the following environment
variables stored in either a `.env` file, or available as CI variables:

```bash
# your google cloud provisioned key
GOOGLE_API_KEY=ABC123

# the string of letters / numbers after docs.google.com/spreadsheets/d/*
SPREADSHEET_ID=ABCYXZ123456

# the appended url for you api.
# ie api.hostname.com/API_ROOT/handler_path
API_ROOT=test

# the name of your AWS certificate
CERT_NAME=example.certificate.name

# your domain name without any subdomains
# This project will automatically create subdomains api.*.* and staging-api.*.*
# To change this see the custom variables at the bottom of `serverless.yml`.
DOMAIN_ROOT=example.com
```

Next simply run

```bash
make install

# this step will take ~30mins to take effect (deploys an api gateway domain)
make domain
```

## Deploy

To deploy this locally simply run

```bash
make deploy-staging
# or
make deploy-production
```

See the `Makefile` for further details.

## Debugging

For local debugging run

```bash
make run
```

to run a local deployment at `localhost:4000`.

## Tests

This project includes the `jest` test runner. To run any tests simply run

```bash
make test
```

Jest will automatically run any tests matching the default `*.(spec|test).js`
regular expression.

See [docs](https://facebook.github.io/jest/docs/en/getting-started.html)
for further details

## CI

This project comes with a configuration for gitlab's CI. To use it, you'll need
to add the `.env` variables to the repo's secret variables. See
[instructions](https://docs.gitlab.com/ee/ci/variables/#secret-variables) for
details.

## Common issues

- `no access` :
 Ensure that you're trying to access an 'open' google sheet (see
[Google Sheets](#google-sheets))

## TODO

- [x] move hardcoded domains into `.env` file
- [ ] refactor handler methods to use sheets queries as params
- [ ] configure handler path to be a restfull-path for the google sheet table
    - ie `GET` `/$BASE_API/table_name` is not predefinined and instead instructs
    the handler to fetch a specific table (`table_name`)
- [ ] add `POST` support
- [ ] add graphql endpoint/layer


## Additional Documentation
[Google Sheets Getting Started](https://developers.google.com/sheets/api/quickstart/nodejs)

[Google Sheets API Reference](https://developers.google.com/sheets/api/reference/rest/)

[Gitlab CI Quick Start](https://docs.gitlab.com/ee/ci/quick_start/README.html)
