.PHONY: test, install, domain, delete-domain, deploy-staging, deploy-production

ifndef CI
	include .env
	export $(shell sed 's/=.*//' .env)
endif

install:
	yarn -D

test:
	yarn run jest --coverage

run:
	serverless offline start

domain:
	serverless create_domain --stage production
	serverless create_domain --stage staging

clean-domain:
	serverless delete_domain --stage production
	serverless delete_domain --stage staging

deploy-staging:
	serverless deploy --stage staging

deploy-production:
	serverless deploy --stage production
