'use strict';
const { google } = require('googleapis');
const _ = require('lodash');

const parseSheetsData = ({data}) => {
  const keys = data.values[0];
  const values = _.slice(data.values, 1);
  let dataObj = [];
  _.forEach(values, (row) => {
    let obj = {};
    _.forEach(keys, (key, index) => (obj[key] = row[index]));
    dataObj.push(obj);
  });
  return dataObj;
};

const getQuerySheets = (query, callback) => {
  const sheets = google.sheets({ version: 'v4', auth: process.env.GOOGLE_API_KEY });
  sheets.spreadsheets.values.get({
    spreadsheetId: process.env.SPREADSHEET_ID,
    range: query
  }, (err, data) => { callback(err, !err ? parseSheetsData(data) : null) });
};

const responseData = (data) => {
  return {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    statusCode: 200,
    body: JSON.stringify({
      data
    }),
  };
};

module.exports = { responseData, getQuerySheets };
