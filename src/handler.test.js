const { getKits, getQuestions, getAnswers } = require('./handler.js');

const pprint = (obj) => { console.log(JSON.stringify(obj, null, 2)); };
const printResp = (err, resp) => { pprint(err ? err : JSON.parse(resp.body)) };

describe('getKits', () => {
  it('should run without errors', () => {
    getKits(null, null, () => ({}));
  });
});

describe('getQuestions', () => {
  it('should run without errors', () => {
    getQuestions(null, null, () => ({}));
  });
});

describe('getAnswers', () => {
  it('should run without errors', () => {
    getAnswers(null, null, () => ({}));
  });
});
