'use strict';

const { getQuerySheets, responseData } = require('./util')

module.exports.getKits = (event, context, callback) => {
  getQuerySheets('Kits!A1:F', (err, data) => {
    if (err) {
      callback(err);
    }
    else {
      callback(null, responseData(data));
    }
  });
};

module.exports.getQuestions = (event, context, callback) => {
  getQuerySheets('Questions!A1:E', (err, data) => {
    if (err) {
      callback(err);
    }
    else {
      callback(null, responseData(data));
    }
  });
};

module.exports.getAnswers = (event, context, callback) => {
  getQuerySheets('Answers!A1:O', (err, data) => {
    if (err) {
      callback(err);
    }
    else {
      callback(null, responseData(data));
    }
  });
};
